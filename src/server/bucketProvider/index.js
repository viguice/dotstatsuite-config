import GoogleProvider from './googleProvider';
import MinioProvider from './minioProvider';
import FileSystemProvider from './fileSystemProvider';
import AzureProvider from './azureProvider';
import AwsProvider from './awsProvider';

export default ctx => {
  const { config } = ctx();

  switch(config.bucketsProvider.name){
    case 'minio': return MinioProvider(config)
    case 'gke': return GoogleProvider(config)
    case 'fs': return FileSystemProvider(config)
    case 'azure': return AzureProvider(config)
    case 'aws': return AwsProvider(config)
  }
}
