import { BlobServiceClient, StorageSharedKeyCredential } from '@azure/storage-blob'
import path from 'path'

const downloadFile = (containerClient, bucketname) => async id => {  
  const filePath = path.join(bucketname, id);
  const blobClient = containerClient.getBlobClient(filePath);
  const blockBlobClient = blobClient.getBlockBlobClient();
  const downloadResponse = await blockBlobClient.download(0);
  return downloadResponse.readableStreamBody
};

export default config => {
    const conf = {
        accountName: config.bucketsProvider.accountName,
        accountKey: config.bucketsProvider.accountKey,
        sas: config.bucketsProvider.sas,
        containerName: config.bucketsProvider.containerName,
  };
  const sharedKeyCredential = new StorageSharedKeyCredential(conf.accountName, conf.accountKey);
  // Use either Account Key or SAS TOKEN
  const blobServiceClient = (
    conf.accountKey
    ? new BlobServiceClient(`https://${conf.accountName}.blob.core.windows.net`, sharedKeyCredential)
    : new BlobServiceClient(`https://${conf.accountName}.blob.core.windows.net?${conf.sas}`)
  );

  const containerClient = blobServiceClient.getContainerClient(conf.containerName);
    const res = bucketname => {
      return {
        name: 'azure',
        downloadFile: downloadFile(containerClient, bucketname),
      };
    };
  
    return res;
};
