import BucketProvider from '../bucketProvider';
export default ctx => ctx({ bucketProvider: BucketProvider(ctx) });
